# android-gradle-aliyun-mirrors
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/192739_d0b938b9_2340776.png "6c224f4a20a44623d0bfc7979322720e0df3d7ca.png")

#### 介绍
android studio gradle的国内源配置文件<br>
您可以在下方复制配置后，在用户目录下的.gradle文件夹下创建init.gradle文件，粘贴保存即可

```
allprojects{
    repositories {
        def ALIYUN_REPOSITORY_URL = 'http://maven.aliyun.com/nexus/content/groups/public'
        def ALIYUN_JCENTER_URL = 'http://maven.aliyun.com/nexus/content/repositories/jcenter'
        all { ArtifactRepository repo ->
            if(repo instanceof MavenArtifactRepository){
                def url = repo.url.toString()
                if (url.startsWith('https://repo1.maven.org/maven2')) {
                    project.logger.lifecycle "Repository ${repo.url} replaced by $ALIYUN_REPOSITORY_URL."
                    remove repo
                }
                if (url.startsWith('https://jcenter.bintray.com/')) {
                    project.logger.lifecycle "Repository ${repo.url} replaced by $ALIYUN_JCENTER_URL."
                    remove repo
                }
            }
        }
        maven {
                url ALIYUN_REPOSITORY_URL
            url ALIYUN_JCENTER_URL
        }
    }
}
```




#### 安装教程

您可以在下方复制配置后，在用户目录下的.gradle文件夹下创建init.gradle文件，粘贴保存即可

```
allprojects{
    repositories {
        def ALIYUN_REPOSITORY_URL = 'http://maven.aliyun.com/nexus/content/groups/public'
        def ALIYUN_JCENTER_URL = 'http://maven.aliyun.com/nexus/content/repositories/jcenter'
        all { ArtifactRepository repo ->
            if(repo instanceof MavenArtifactRepository){
                def url = repo.url.toString()
                if (url.startsWith('https://repo1.maven.org/maven2')) {
                    project.logger.lifecycle "Repository ${repo.url} replaced by $ALIYUN_REPOSITORY_URL."
                    remove repo
                }
                if (url.startsWith('https://jcenter.bintray.com/')) {
                    project.logger.lifecycle "Repository ${repo.url} replaced by $ALIYUN_JCENTER_URL."
                    remove repo
                }
            }
        }
        maven {
                url ALIYUN_REPOSITORY_URL
            url ALIYUN_JCENTER_URL
        }
    }
}
```


或者克隆（下载本仓库的zip包）将init.gradle复制到用户目录即可！

### 开源许可协议
本仓库采用WTFPL许可协议<br>
<a herf="https://gitee.com/ForMat1_admin/android-gradle-aliyun-mirrors/blob/master/LICENSE">查看开源许可协议</a>


